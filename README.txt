
-- SUMMARY --

The CSE(Custom Search Engine) is a client side search module, which provide
many great features like: Chinese content search, full-text search and keywords
highlight.


-- REQUIREMENTS --

None.


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/895232 for further information.


-- CONFIGURATION --

* Configure CSE settings in Configurations » Search and metadata » CSE Settings:

  - CSE Server

    Server URL which provide custom search engine, default use
    http://www.theriseit.com

  - CSE AppID & CSE AppKey

    AppID and AppKey info of your CSE, you can apply from
    http://www.theriseit.com#contact

　　To take a test run of this module, use follow setting:
    CSE AppID: 17
    CSE AppKey: 58823f3463c5ca13f41f79678aff661d
    
-- USAGE --
* Go to search/cse page, type keyword and search.
